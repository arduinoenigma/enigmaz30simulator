// EnigmaZ30Sim.ino - CharMap.ino
// @arduinoenigma 2019

//
// 0 a g d b c f e
// 0 1 1 1 1 1 1 1 // digit
// 1 1 1 1 1 1 1 0 // shift left
// 0 1 1 0 1 0 1 0 // and with mask
// 0 1 1 0 1 0 1 0 // digit scrolled one step up
//
// 0 a g d b c f e
// 0 1 1 1 1 1 1 1 // digit
// 0 0 1 1 1 1 1 1 // shift right
// 0 0 1 1 0 1 0 1 // and with mask
// 0 0 1 1 0 1 0 1 // digit scrolled one step dn
//

#define RawBits0 95
#define RawBits1 12
#define RawBits2 121
#define RawBits3 124
#define RawBits4 46
#define RawBits5 118
#define RawBits6 119
#define RawBits7 76
#define RawBits8 127
#define RawBits9 126

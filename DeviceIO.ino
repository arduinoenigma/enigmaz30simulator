// EnigmaZ30Sim.ino - DeviceIO.ino
// @arduinoenigma 2019

#if defined(CommonCathode)
#define SegmentOn high
#define SegmentOff low
#define DigitOn low
#define DigitOff high
#define LampsOn low
#define LampsOff high
#endif

#if defined(CommonAnode)
#define SegmentOn low
#define SegmentOff high
#define DigitOn high
#define DigitOff low
#define LampsOn low
#define LampsOff high
#endif

//
// A0 = D14
// A1 = D15
// A2 = D16
// A3 = D17
// A4 = D18
// A5 = D19
// A6 = D20
// A7 = D21
//

GPIO<BOARD::D2> CCDS3;
GPIO<BOARD::D3> CCDS1;
GPIO<BOARD::D4> UPDNL;
GPIO<BOARD::D5> LED1L;
GPIO<BOARD::D6> SegmentC;
GPIO<BOARD::D7> SegmentD;
GPIO<BOARD::D8> SegmentE;
GPIO<BOARD::D9> SegmentB;
GPIO<BOARD::D10> SegmentA;
GPIO<BOARD::D11> SegmentF;
GPIO<BOARD::D12> SegmentG;
GPIO<BOARD::D14> KEY2L;
GPIO<BOARD::D15> KEY1L;
GPIO<BOARD::D16> LED2L;
GPIO<BOARD::D17> CCDS2;
GPIO<BOARD::D18> CCDS4;
GPIO<BOARD::D19> SegmentDP;


void allSegmentOutput() {
  SegmentA.output();
  SegmentB.output();
  SegmentC.output();
  SegmentD.output();
  SegmentE.output();
  SegmentF.output();
  SegmentG.output();
  SegmentDP.output();
}


void allSegmentInput() {
  SegmentA.input();
  SegmentB.input();
  SegmentC.input();
  SegmentD.input();
  SegmentE.input();
  SegmentF.input();
  SegmentG.input();
  SegmentDP.input();
}


void allSegmentOff() {
  SegmentA.SegmentOff();
  SegmentB.SegmentOff();
  SegmentC.SegmentOff();
  SegmentD.SegmentOff();
  SegmentE.SegmentOff();
  SegmentF.SegmentOff();
  SegmentG.SegmentOff();
  SegmentDP.SegmentOff();
}


void allSegmentHigh() {
  SegmentA.high();
  SegmentB.high();
  SegmentC.high();
  SegmentD.high();
  SegmentE.high();
  SegmentF.high();
  SegmentG.high();
  SegmentDP.high();
}


void allDigitsOutput() {
  CCDS1.output();
  CCDS2.output();
  CCDS3.output();
  CCDS4.output();
}


void allDigitsInput() {
  CCDS1.input();
  CCDS2.input();
  CCDS3.input();
  CCDS4.input();
}


void allDigitsOff() {
  CCDS1.DigitOff();
  CCDS2.DigitOff();
  CCDS3.DigitOff();
  CCDS4.DigitOff();
}


void allLampsOutput() {
  LED1L.output();
  LED2L.output();
}


void allLampsInput()
{
  LED1L.input();
  LED2L.input();
}


void allLampsOff()
{
  LED1L.LampsOff();
  LED2L.LampsOff();
}


void allKeysInput()
{
  KEY1L.input();
  KEY1L.high();

  KEY2L.input();
  KEY2L.high();

  UPDNL.input();
  UPDNL.high();
}

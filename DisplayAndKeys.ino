// EnigmaZ30Sim.ino - DisplayAndKeys.ino
// @arduinoenigma 2019

// current value to display
byte DisplayValues[4] = {0, 0, 0, 0};
// the next value to display
byte NewDisplayValues[4] = {0, 0, 0, 0};

byte ScrollState[4] = {0, 0, 0, 0};
byte ScrollDelay[4] = {0, 0, 0, 0};
byte NeedsScroll[4] = {0, 0, 0, 0};

byte RawDisplayValues[6] = {0, 0, 0, 0, 0, 0};

byte KeysPressedCount = 0;

byte InhibitLampfield = 2;

byte BrightnessLevel = 2;

//needs function that outputs 1 bit at a time and reads the keys for that bit, store in KeyPressed.
//every time a key is detected, store it here according to its position A..Z DS1u,DS2u,DS3u,DS4u,DS1d,DS2d,DS3d,DS4d,MENU
byte KeysPressed[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
// every time a key is stored in KeyPressed, also put a 255 in here. a Debounce() function will decrement and clear the KeysPressed entry when this geto to 0
byte KeysDebounce[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
// compare KeysPressed with KeysProcessed, if non-zero in KeysPressed and zero here, store it here and process it, if zero in KeysPressed, zero it here.
byte KeysProcessed[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// store here what lamp each key lit up so we can turn it off later
byte LightUp[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


void selectDigit(byte digit) {

  allDigitsInput();
  allLampsInput();

  allDigitsOff();
  allLampsOff();

  switch (digit) {
    case 1:
      CCDS1.DigitOn();
      CCDS1.output();
      break;
    case 2:
      CCDS2.DigitOn();
      CCDS2.output();
      break;
    case 3:
      CCDS3.DigitOn();
      CCDS3.output();
      break;
    case 4:
      CCDS4.DigitOn();
      CCDS4.output();
      break;
    case 5:
      LED1L.LampsOn();
      LED1L.output();
      break;
    case 6:
      LED2L.LampsOn();
      LED2L.output();
      break;
  }
}


void displayRaw(byte raw)
{
  allSegmentOutput();

  if (raw & B10000000)
  {
    SegmentDP.SegmentOn();
  }
  else
  {
    SegmentDP.SegmentOff();
  }

  if (raw & B01000000)
  {
    SegmentA.SegmentOn();
  }
  else
  {
    SegmentA.SegmentOff();
  }

  if (raw & B00100000)
  {
    SegmentG.SegmentOn();
  }
  else
  {
    SegmentG.SegmentOff();
  }

  if (raw & B00010000)
  {
    SegmentD.SegmentOn();
  }
  else
  {
    SegmentD.SegmentOff();
  }

  if (raw & B00001000)
  {
    SegmentB.SegmentOn();
  }
  else
  {
    SegmentB.SegmentOff();
  }

  if (raw & B00000100)
  {
    SegmentC.SegmentOn();
  }
  else
  {
    SegmentC.SegmentOff();
  }

  if (raw & B00000010)
  {
    SegmentF.SegmentOn();
  }
  else
  {
    SegmentF.SegmentOff();
  }

  if (raw & B00000001)
  {
    SegmentE.SegmentOn();
  }
  else
  {
    SegmentE.SegmentOff();
  }
}


// give it a symbol (0..9) and it returns an 8 bit value for the display
uint8_t getRaw(byte letter)
{
  switch (letter)
  {
    case 0:
      return RawBits0;
    case 1:
      return RawBits1;
    case 2:
      return RawBits2;
    case 3:
      return RawBits3;
    case 4:
      return RawBits4;
    case 5:
      return RawBits5;
    case 6:
      return RawBits6;
    case 7:
      return RawBits7;
    case 8:
      return RawBits8;
    case 9:
      return RawBits9;
  }
  return 0;
}


//LED1L is in RawDisplayValues[4]
//LED2L is in RawDisplayValues[5]
void lightOn(byte digit)
{
  switch (digit)
  {
    case 0:
      RawDisplayValues[5] = RawDisplayValues[5] | B00001000;
      break;
    case 1:
      RawDisplayValues[4] = RawDisplayValues[4] | B01000000;
      break;
    case 2:
      RawDisplayValues[4] = RawDisplayValues[4] | B00000001;
      break;
    case 3:
      RawDisplayValues[4] = RawDisplayValues[4] | B00000010;
      break;
    case 4:
      RawDisplayValues[4] = RawDisplayValues[4] | B00010000;
      break;
    case 5:
      RawDisplayValues[4] = RawDisplayValues[4] | B00100000;
      break;
    case 6:
      RawDisplayValues[5] = RawDisplayValues[5] | B00010000;
      break;
    case 7:
      RawDisplayValues[5] = RawDisplayValues[5] | B00100000;
      break;
    case 8:
      RawDisplayValues[5] = RawDisplayValues[5] | B00000100;
      break;
    case 9:
      RawDisplayValues[5] = RawDisplayValues[5] | B10000000;
      break;
  }
}


//LED1L is in RawDisplayValues[4]
//LED2L is in RawDisplayValues[5]
void lightOff(byte digit)
{
  switch (digit)
  {
    case 0:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11110111; // B00001000;
      break;
    case 1:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b10111111; // B01000000;
      break;
    case 2:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111110; // B00000001;
      break;
    case 3:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11111101; // B00000010;
      break;
    case 4:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11101111; // B00010000;
      break;
    case 5:
      RawDisplayValues[4] = RawDisplayValues[4] & 0b11011111; // B00100000;
      break;
    case 6:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11101111; // B00010000;
      break;
    case 7:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11011111; // B00100000;
      break;
    case 8:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b11111011; // B00000100;
      break;
    case 9:
      RawDisplayValues[5] = RawDisplayValues[5] & 0b01111111; // B10000000;
      break;
  }
}


void allLightsOff()
{
  RawDisplayValues[4] = 0;
  RawDisplayValues[5] = 0;
}


void readKeys()
{
  static byte state = 0;

  //turn off all displays
  allDigitsInput();
  allLampsInput();

  allDigitsOff();
  allLampsOff();

  // to detect keys we will make one segment 0 at a time to see if it comes in on KEY1L...
  // we cannot depend on DisplayRaw since the polarity can change with display type...
  allSegmentInput(); // instead of putting 1 on the unselected pins, make them float so two keys can be pushed
  allSegmentHigh(); // in addition to unused pins being in HighZ, they also need to be pullup

  state++;
  if (state == 9)
  {
    state = 1;
  }

  switch (state)
  {
    case 1:
      SegmentDP.output();
      SegmentDP.low();
      delayMicroseconds(2); // don't like it, but we must have a settling time to allow the segment to go low
      processKeyBus(KEY2L.read(), '9');
      processKeyBus(UPDNL.read(), 'd'); //DS4dn
      break;
    case 2:
      SegmentA.output();
      SegmentA.low();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), '1');
      processKeyBus(UPDNL.read(), 'A'); //DS1up
      break;
    case 3:
      SegmentG.output();
      SegmentG.low();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), '5');
      processKeyBus(KEY2L.read(), '7');
      processKeyBus(UPDNL.read(), 'C'); //DS3up
      break;
    case 4:
      SegmentD.output();
      SegmentD.low();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), '4');
      processKeyBus(KEY2L.read(), '6');
      processKeyBus(UPDNL.read(), 'b'); //DS2dn
      break;
    case 5:
      SegmentB.output();
      SegmentB.low();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), 'E'); //Menu
      processKeyBus(KEY2L.read(), '0');
      processKeyBus(UPDNL.read(), 'D'); //DS4up
      break;
    case 6:
      SegmentC.output();
      SegmentC.low();
      delayMicroseconds(2);
      processKeyBus(KEY2L.read(), '8');
      processKeyBus(UPDNL.read(), 'c'); //DS3dn
      break;
    case 7:
      SegmentF.output();
      SegmentF.low();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), '3');
      processKeyBus(UPDNL.read(), 'B'); //DS2up
      break;
    case 8:
      SegmentE.output();
      SegmentE.low();
      delayMicroseconds(2);
      processKeyBus(KEY1L.read(), '2');
      processKeyBus(UPDNL.read(), 'a'); //DS1dn
      break;
  }

  allSegmentOutput();
  allSegmentOff();
}


void processKeyBus(bool BusValue, byte key)
{
  if (BusValue == 0)
  {
    byte index = getKeyIndex(key);
    KeysPressed[index] = key;
    KeysDebounce[index] = 3;
  }
}


//0..9 ABCDE abcd (up keys and menu) abcd (down keys)
byte getKeyIndex(byte key)
{
  byte index = 0;
  if ((key > '0' - 1) && (key < '9' + 1))
  {
    index = key - '0';
  }
  else if ((key > 'A' - 1) && (key < 'E' + 1))
  {
    index = key - 'A' + 10;
  }
  else if ((key > 'a' - 1) && (key < 'd' + 1))
  {
    index = key - 'a' + 10 + 5;
  }
  return index;
}


void debounceKeys()
{
  static byte state = 0;

  state++;
  if (state == 19)
  {
    state = 0;
  }

  if (KeysDebounce[state])
  {
    --KeysDebounce[state];
    if (KeysDebounce[state] == 0)
    {
      byte light = LightUp[state];
      if (light != 0)
      {
        lightOff(light - '0');
      }

      KeysPressed[state] = 0;
      KeysProcessed[state] = 0;
      LightUp[state] = 0;
      KeysPressedCount--;
    }
  }
}


void processKeys()
{
  static byte state = 0;

  state++;
  if (state == 19)
  {
    state = 0;
  }

  if ((KeysPressed[state]) && (KeysProcessed[state] == 0))
  {
    KeysProcessed[state] = KeysPressed[state];
    doAction(KeysPressed[state]);
    KeysPressedCount++;
  }
}


void LampBrightnessDelay(byte level)
{
  switch (level)
  {
    case 0:
      delayMicroseconds(25);
      break;
    case 1:
      delayMicroseconds(50);
      break;
    case 2:
      delayMicroseconds(100);
      break;
    case 3:
      delayMicroseconds(200);
      break;
    default:
      delayMicroseconds(350);
      break;
  }
}

void IncreaseBrightness()
{
  if (BrightnessLevel < 4)
  {
    BrightnessLevel++;
  }
}

void DecreaseBrightness()
{
  if (BrightnessLevel > 0)
  {
    BrightnessLevel--;
  }

}

void updateRawValues()
{
  RawDisplayValues[0] = getRaw(DisplayValues[0]);
  RawDisplayValues[1] = getRaw(DisplayValues[1]);
  RawDisplayValues[2] = getRaw(DisplayValues[2]);
  RawDisplayValues[3] = getRaw(DisplayValues[3]);
}


void updateDisplay()
{
  static byte state = 0;
  static byte statescroll = 0;

  byte src = DisplayValues[statescroll];
  byte dst = NewDisplayValues[statescroll];

  if ((src != dst) && (NeedsScroll[statescroll] == 0))
  {
    NeedsScroll[statescroll] = 1;
    ScrollState[statescroll] = 0;
    ScrollDelay[statescroll] = 0;
  }

  if (ScrollState[statescroll] != 5)
  {
    InhibitLampfield = 0;

    ++ScrollDelay[statescroll];
    if (ScrollDelay[statescroll] > 80) // 80
    {
      if (((dst > src) || ((src == 9) && (dst == 0))) && !((src == 0) && (dst == 9)))
      {
        RawDisplayValues[statescroll] = stepUp(src, dst, ScrollState[statescroll]);
      }
      else if ((dst < src) || ((src == 0) && (dst == 9)))
      {
        RawDisplayValues[statescroll] = stepDown(src, dst, ScrollState[statescroll]);
      }

      ScrollDelay[statescroll] = 0;
      ++ScrollState[statescroll];

      if (ScrollState[statescroll] == 5)
      {
        NeedsScroll[statescroll] = 0;
        DisplayValues[statescroll] = NewDisplayValues[statescroll];
        RawDisplayValues[statescroll] = getRaw(DisplayValues[statescroll]);
      }
    }
  }

  //put this after the scroll logic so the code sees it from 0..3
  statescroll++;
  if (statescroll == 4)
  {
    statescroll = 0;
    //logic to detect when scrolling is finished
    if (InhibitLampfield < 2)
    {
      InhibitLampfield++;
    }
  }

  //put this one here so the display logic sees it from 1..10
  state++;
  if (state == 11)
  {
    state = 1;
  }

  switch (state)
  {
    case 1:
      selectDigit(1);
      displayRaw(RawDisplayValues[0] & 0B00001111);  // splitting each digit in two equalizes the brightness between 1/7 and 8/9
      //displayRaw(getRaw(DisplayValues[0]) & 0B00001111);  // splitting each digit in two equalizes the brightness between 1/7 and 8/9
      delayMicroseconds(200);
      break;
    case 2:
      selectDigit(2);
      displayRaw(RawDisplayValues[1] & 0B00001111);
      //displayRaw(getRaw(DisplayValues[1]) & 0B00001111);
      delayMicroseconds(200);
      break;
    case 3:
      selectDigit(3);
      displayRaw(RawDisplayValues[2] & 0B00001111);
      //displayRaw(getRaw(DisplayValues[2]) & 0B00001111);
      delayMicroseconds(200);
      break;
    case 4:
      selectDigit(4);
      displayRaw(RawDisplayValues[3] & 0B00001111);
      //displayRaw(getRaw(DisplayValues[3]) & 0B00001111);
      delayMicroseconds(200);
      break;

    case 5:
      selectDigit(1);
      displayRaw(RawDisplayValues[0] & 0B11110000); // second half
      //displayRaw(getRaw(DisplayValues[0]) & 0B11110000); // second half
      delayMicroseconds(200);
      break;
    case 6:
      selectDigit(2);
      displayRaw(RawDisplayValues[1] & 0B11110000);
      //displayRaw(getRaw(DisplayValues[1]) & 0B11110000);
      delayMicroseconds(200);
      break;
    case 7:
      selectDigit(3);
      displayRaw(RawDisplayValues[2] & 0B11110000);
      //displayRaw(getRaw(DisplayValues[2]) & 0B11110000);
      delayMicroseconds(200);
      break;
    case 8:
      selectDigit(4);
      displayRaw(RawDisplayValues[3] & 0B11110000);
      //displayRaw(getRaw(DisplayValues[3]) & 0B11110000);
      delayMicroseconds(200);
      break;

    case 9:
      selectDigit(5);                       // lampfield
      if (InhibitLampfield == 2)
      {
        displayRaw(RawDisplayValues[4]);
      }
      LampBrightnessDelay(BrightnessLevel);               // BUG: this gets changed to a variable, configurable via menu  "bL 1..5"
      break;
    case 10:
      selectDigit(6);
      if (InhibitLampfield == 2)
      {
        displayRaw(RawDisplayValues[5]);
      }
      LampBrightnessDelay(BrightnessLevel);
      break;
  }

  selectDigit(99); // turn display off
}

// EnigmaZ30Sim.ino - Enigma.ino
// @arduinoenigma 2019


#define ringsettingkeyidx 3
#define rotorkeyidx 7
#define keyinoutidx 11

byte enigmakey[] = {
  3, 2, 1, 1, 1, 1, 1, 8, 8, 8, 8, 0, 0
  //1, 2, 3, 1, 1, 1, 1, 8, 8, 8, 8, 0, 0
  //1, 2, 3, 1, 6, 7, 8, 4, 9, 8, 6, 0, 0
  //1, 2, 3, 1, 6, 7, 8, 4, 9, 8, 9, 0, 0
  //1, 2, 3, 4, 5, 6, 7, 8, 8, 8, 8, 0, 0
};

const byte rotorindex[] PROGMEM = {
  30, 0, 10, 20
};

const byte rotors[] PROGMEM = {
  9, 6, 4, 1, 8, 2, 7, 0, 3, 5,
  2, 5, 8, 4, 1, 0, 9, 7, 6, 3,
  4, 3, 5, 8, 1, 6, 2, 0, 7, 9,
  2, 5, 0, 7, 9, 1, 8, 3, 6, 4,
  7, 3, 5, 8, 2, 9, 1, 6, 4, 0,
  5, 4, 0, 9, 3, 1, 8, 7, 2, 6,
  7, 4, 6, 1, 0, 2, 5, 8, 3, 9,
};

byte KeyPressed = 0;
byte KeyIn = 0;

byte SetType = 0;
byte SetRing = 0;
byte SetWheel = 0;
byte SetStep = 0;

byte printmode = 0;

byte stepmode = 1; // lever stepping


byte calculateRing(byte KeyIn,  byte RotorIdx)
{
  char t = 0;  // signed type

  t = KeyIn + enigmakey[rotorkeyidx + RotorIdx] + 1;

  if (t > 9) t = t - 10;

  t = t - enigmakey[ringsettingkeyidx + RotorIdx];

  if (t < 0) t = t + 10;

  return t;
}

// ring setting affects stepping point
// https://people.physik.hu-berlin.de/~palloks/js/enigma/enigma-z_v261_en.html
//
byte steppoint(byte rotor)
{
  byte p = 9 + enigmakey[ringsettingkeyidx + rotor] - 1;

  if (p > 9) p = p - 10;

  return p;
}

void steprotor() {

  if (stepmode == 1)
  {
    //lever stepping
    byte step[5];

    step[0] = 0;
    for (char i = 1; i < 4; i++) {
      step[i] = 0;
      //if (enigmakey[rotorkeyidx + i] == 9) { //BUG: step point fixed at 9
      if (enigmakey[rotorkeyidx + i] == steppoint(i)) {
        step[i] = 1;
      }
    }
    step[4] = 1;

    for (char i = 0; i < 4; i++) {
      if (step[i] || step[i + 1]) {
        enigmakey[rotorkeyidx + i]++;
        if (enigmakey[rotorkeyidx + i] > 9) {
          enigmakey[rotorkeyidx + i] = 0;
        }
      }
    }
  }
  else
  {
    //gear stepping
    byte keepstepping = 1;
    byte prevstepping = 1;
    byte index = 3;

    do
    {
      if (enigmakey[rotorkeyidx + index] != 9)
      {
        keepstepping = 0;
      }

      if ((prevstepping) || (index == 3)) {
        enigmakey[rotorkeyidx + index]++;
        if (enigmakey[rotorkeyidx + index] > 9) {
          enigmakey[rotorkeyidx + index] = 0;
        }
      }

      prevstepping = keepstepping;

      index--;

    } while (index != 255);
  }
}


byte enigma(byte KeyIn)
{
  char t = KeyIn;
  char nextrotor = -1;
  char currrotor = 3;
  char currrotortype;
  char reverserotor = 0;

  enigmakey[keyinoutidx] = KeyIn;

  //steprotor(); // disabled and called externally upon first key press, simulates multiple key presses taking different paths through stationary rotors

  for (char i = 0; i < 7; i++) {

    t = calculateRing(t, currrotor);

    currrotortype = 0;
    if (currrotor != 0) {
      currrotortype = enigmakey[currrotor - 1];
    }

    currrotortype =  pgm_read_byte_near(rotorindex + currrotortype);

    t = pgm_read_byte_near(rotors + currrotortype + reverserotor + t);

    currrotortype = calculateRing(0, currrotor);
    t = t - currrotortype;
    if (t < 0) {
      t += 10;
    }

    currrotor += nextrotor;

    if (i == 2) {
      nextrotor = 1;
    }

    if (i == 3) {
      reverserotor = 40;
    }
  }

  enigmakey[keyinoutidx + 1 ] = t;

  return t;
}


void settype(int type) {
  for (byte i = 0; i < 3; i++) {
    enigmakey[2 - i] = type % 10;
    type = type / 10;
  }
}


void setrings(int rings) {
  for (byte i = 0; i < 4; i++) {
    enigmakey[ringsettingkeyidx + 3 - i] = rings % 10;
    rings = rings / 10;
  }
}


void setwheel(int wheel) {
  for (byte i = 0; i < 4; i++) {
    enigmakey[rotorkeyidx + 3 - i] = wheel % 10;
    wheel = wheel / 10;
  }
}

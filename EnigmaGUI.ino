// EnigmaZ30Sim.ino - EnigmaGUI.ino
// @arduinoenigma 2019


void ShowMenu(byte menulevel)
{
  static byte prevrotor[4];

  switch (menulevel)
  {
    case 0:
      {
        prevrotor[0] = DisplayValues[0];
        prevrotor[1] = DisplayValues[1];
        prevrotor[2] = DisplayValues[2];
        prevrotor[3] = DisplayValues[3];

        DisplayValues[0] = 99;
        DisplayValues[1] = enigmakey[0];
        DisplayValues[2] = enigmakey[1];
        DisplayValues[3] = enigmakey[2];

        NewDisplayValues[0] = DisplayValues[0];
        NewDisplayValues[1] = DisplayValues[1];
        NewDisplayValues[2] = DisplayValues[2];
        NewDisplayValues[3] = DisplayValues[3];

        updateRawValues();

        allLightsOff();
        RawDisplayValues[4] = 255;
        RawDisplayValues[5] = 255;
        lightOff(1);
        break;
      }

    case 1:
      {
        enigmakey[0] = DisplayValues[1];
        enigmakey[1] = DisplayValues[2];
        enigmakey[2] = DisplayValues[3];

        DisplayValues[0] = enigmakey[3];
        DisplayValues[1] = enigmakey[4];
        DisplayValues[2] = enigmakey[5];
        DisplayValues[3] = enigmakey[6];

        NewDisplayValues[0] = DisplayValues[0];
        NewDisplayValues[1] = DisplayValues[1];
        NewDisplayValues[2] = DisplayValues[2];
        NewDisplayValues[3] = DisplayValues[3];

        updateRawValues();
        lightOn(1);
        lightOff(2);

        break;
      }

    case 2:
      {
        enigmakey[3] = DisplayValues[0];
        enigmakey[4] = DisplayValues[1];
        enigmakey[5] = DisplayValues[2];
        enigmakey[6] = DisplayValues[3];

        DisplayValues[3] = stepmode - 1;

        NewDisplayValues[0] = DisplayValues[0];
        NewDisplayValues[1] = DisplayValues[1];
        NewDisplayValues[2] = DisplayValues[2];
        NewDisplayValues[3] = DisplayValues[3];

        updateRawValues();

        lightOn(2);
        lightOff(3);

        break;
      }

    case 3:
      {
        stepmode = DisplayValues[3] + 1;

        DisplayValues[0] = prevrotor[0];
        DisplayValues[1] = prevrotor[1];
        DisplayValues[2] = prevrotor[2];
        DisplayValues[3] = prevrotor[3];
        NewDisplayValues[0] = DisplayValues[0];
        NewDisplayValues[1] = DisplayValues[1];
        NewDisplayValues[2] = DisplayValues[2];
        NewDisplayValues[3] = DisplayValues[3];

        updateRawValues();
        allLightsOff();

        break;
      }
  }
}


void doAction(byte key)
{
  //keys:
  //A B C D    E
  //a b c d

  static byte inmenu = 0;
  static byte menulevel = 0;
  static byte stayinmenu = 0;

  switch (inmenu)
  {
    case 0:

      if (key == 'E')
      {
        inmenu = 1;
        menulevel = 0;
        stayinmenu = 1;

        ShowMenu(menulevel);
      }
      else
      {
        ChangeRotor(key);
        DoEnigmaGUI(key);
      }

      break;

    case 1:

      if ((key == 'E') && (stayinmenu == 0))
      {
        menulevel++;

        ShowMenu(menulevel);

        if (menulevel == 3)
        {
          inmenu = 0;
        }
      }

      switch (menulevel)
      {
        case 0:

          ChangeRotor(key);

          DisplayValues[0] = 99;
          NewDisplayValues[0] = 99;

          if (NewDisplayValues[1] > 3)
          {
            DisplayValues[1] = 3;
            NewDisplayValues[1] = 1;
          }
          if (NewDisplayValues[2] > 3)
          {
            DisplayValues[2] = 3;
            NewDisplayValues[2] = 1;
          }
          if (NewDisplayValues[3] > 3)
          {
            DisplayValues[3] = 3;
            NewDisplayValues[3] = 1;
          }

          if (NewDisplayValues[1] == 0)
          {
            DisplayValues[1] = 1;
            NewDisplayValues[1] = 3;
          }
          if (NewDisplayValues[2] == 0)
          {
            DisplayValues[2] = 1;
            NewDisplayValues[2] = 3;
          }
          if (NewDisplayValues[3] == 0)
          {
            DisplayValues[3] = 1;
            NewDisplayValues[3] = 3;
          }

          DisplayValues[0] = NewDisplayValues[0];
          DisplayValues[1] = NewDisplayValues[1];
          DisplayValues[2] = NewDisplayValues[2];
          DisplayValues[3] = NewDisplayValues[3];

          byte checkrotortypes1[3];

          for (byte i = 0; i < 3; i++)
          {
            checkrotortypes1[i] = 0;
          }

          for (byte i = 1; i < 4; i++)
          {
            checkrotortypes1[DisplayValues[i] - 1]++;
          }

          stayinmenu = 0;
          for (byte i = 0; i < 3; i++)
          {
            if (checkrotortypes1[i] != 1)
            {
              stayinmenu = 1;
            }
          }

          if ((stayinmenu == 0) && (key == 'E'))
          {
            menulevel++;
            ShowMenu(menulevel);
          }

          updateRawValues();

          break;

        case 1:

          ChangeRotor(key);

          DisplayValues[0] = NewDisplayValues[0];
          DisplayValues[1] = NewDisplayValues[1];
          DisplayValues[2] = NewDisplayValues[2];
          DisplayValues[3] = NewDisplayValues[3];

          updateRawValues();

          break;

        case 2:

          if ((key == 'D') || (key == 'd'))
          {
            ChangeRotor(key);
          }

          if (key == 'A')
          {
            DecreaseBrightness();
          }

          if (key == 'a')
          {
            IncreaseBrightness();
          }

          if (NewDisplayValues[3] == 2)
          {
            NewDisplayValues[3] = 0;
          }

          if (NewDisplayValues[3] == 9)
          {
            NewDisplayValues[3] = 1;
          }

          DisplayValues[3] = NewDisplayValues[3];

          updateRawValues();

          RawDisplayValues[0] = 0x77;
          RawDisplayValues[1] = 0x73;
          RawDisplayValues[2] = 0x6F;

          break;
      }

      break;
  }
}


void ChangeRotor(byte key)
{
  byte thumbwheelused = 0;

  switch (key)
  {
    case 'A':
      //if pushed in the middle of a transition, finish it
      if (NewDisplayValues[0] != DisplayValues[0])
      {
        DisplayValues[0] = NewDisplayValues[0];
      }
      //then apply the normal logic
      NewDisplayValues[0] = DisplayValues[0] - 1;
      thumbwheelused = 1;
      break;
    case 'a':
      if (NewDisplayValues[0] != DisplayValues[0])
      {
        DisplayValues[0] = NewDisplayValues[0];
      }
      NewDisplayValues[0] = DisplayValues[0] + 1;
      thumbwheelused = 1;
      break;
    case 'B':
      if (NewDisplayValues[1] != DisplayValues[1])
      {
        DisplayValues[1] = NewDisplayValues[1];
      }
      NewDisplayValues[1] = DisplayValues[1] - 1;
      thumbwheelused = 1;
      break;
    case 'b':
      if (NewDisplayValues[1] != DisplayValues[1])
      {
        DisplayValues[1] = NewDisplayValues[1];
      }
      NewDisplayValues[1] = DisplayValues[1] + 1;
      thumbwheelused = 1;
      break;
    case 'C':
      if (NewDisplayValues[2] != DisplayValues[2])
      {
        DisplayValues[2] = NewDisplayValues[2];
      }
      NewDisplayValues[2] = DisplayValues[2] - 1;
      thumbwheelused = 1;
      break;
    case 'c':
      if (NewDisplayValues[2] != DisplayValues[2])
      {
        DisplayValues[2] = NewDisplayValues[2];
      }
      NewDisplayValues[2] = DisplayValues[2] + 1;
      thumbwheelused = 1;
      break;
    case 'D':
      if (NewDisplayValues[3] != DisplayValues[3])
      {
        DisplayValues[3] = NewDisplayValues[3];
      }
      NewDisplayValues[3] = DisplayValues[3] - 1;
      thumbwheelused = 1;
      break;
    case 'd':
      if (NewDisplayValues[3] != DisplayValues[3])
      {
        DisplayValues[3] = NewDisplayValues[3];
      }
      NewDisplayValues[3] = DisplayValues[3] + 1;
      thumbwheelused = 1;
      break;
  }

  if (thumbwheelused == 1)
  {
    for (byte i = 0; i < 4; i++)
    {
      if (NewDisplayValues[i] == 255)
      {
        NewDisplayValues[i] = 9;
      }
      // order is important, check this here as 255 is > 9
      if (NewDisplayValues[i] > 9)
      {
        NewDisplayValues[i] = 0;
      }
    }
  }
}


void DoEnigmaGUI(byte key)
{

  byte updatedisplays = 0;
  byte scrolldisplays = 0;

  //BUG: recalculate enigma value if thumbwheels are used while a number key is pushed
  /*
    case 'E':
    DisplayValues[0] = 0;
    DisplayValues[1] = 0;
    DisplayValues[2] = 0;
    DisplayValues[3] = 0;
    NewDisplayValues[0] = 0;
    NewDisplayValues[1] = 0;
    NewDisplayValues[2] = 0;
    NewDisplayValues[3] = 0;
    allLightsOff();
    updatedisplays = 1;
    break;
  */

  if ((key > '0' - 1) && (key < '9' + 1))
  {
    //resist the temptation of copying NewDisplayValues to DisplayValues, skips visible numbers if keys are pressed during a transition
    enigmakey[rotorkeyidx + 0] = DisplayValues[0];
    enigmakey[rotorkeyidx + 1] = DisplayValues[1];
    enigmakey[rotorkeyidx + 2] = DisplayValues[2];
    enigmakey[rotorkeyidx + 3] = DisplayValues[3];

    //BUG: gets affected by up/down keys and menu
    if (KeysPressedCount == 0)
    {
      steprotor();
    }
    byte o = enigma(key - '0');
    scrolldisplays = 1;

    NewDisplayValues[0] = enigmakey[rotorkeyidx + 0];
    NewDisplayValues[1] = enigmakey[rotorkeyidx + 1];
    NewDisplayValues[2] = enigmakey[rotorkeyidx + 2];
    NewDisplayValues[3] = enigmakey[rotorkeyidx + 3];

    LightUp[getKeyIndex(key)] = o + '0';
    lightOn(o);

    InhibitLampfield = 0; //prevent glitches by doing this early...
  }

  if (updatedisplays == 1)
  {
    updateRawValues();
  }
}

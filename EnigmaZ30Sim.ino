// EnigmaZ30Sim.ino
// @arduinoenigma 2019

// uses this fast pin library:
// https://github.com/mikaelpatel/Arduino-GPIO

//BUGS:
//

#include "GPIO.h"
#include <EEPROM.h>

#define _steptime 341

#define CommonCathode
//#define CommonAnode

#define TASKDUMMYTASK 0

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  Serial.print(F("NanoEnigma Z30 20220731\x0d\x0a"));

  settype(321);
  setrings(1111);
  setwheel(0000); //overriden by the contents of DisplayValues[] in DisplayAndKeys.ino

  allKeysInput();

  //selectDigit() will select the appropriate line to output
  //call with 99 to turn off all common pins and make them inputs
  selectDigit(99);

  allSegmentOff();
  allSegmentOutput();

  //initialize displays
  updateRawValues();

  //testBNumber();

  //testOneSegment();

  //testPressTwoKeys();

  //testDisplay0to9();

  //testScroller();
}


void loop() {
  // put your main code here, to run repeatedly:

  static byte printtimers = 0;

  readKeys();
  debounceKeys();
  processKeys();

  updateDisplay();

  //testDisplayKeysPressed();

  //if (printtimers++ == 0)
  //{
  //DisplayValues[0]++;
  //DisplayValues[1]++;
  //DisplayValues[2]++;
  //DisplayValues[3] = KeysPressedCount;
  //}

  //this is a hack to turn the lights off, affected by up/dn and menu keys
  //if (KeysPressedCount == 0)
  //{
  //allLightsOff();
  //}
}

**# EnigmaZ30Simulator**

**An Enigma Z30 Simulator**

Main file is:
[EnigmaZ30Sim.ino](https://gitlab.com/arduinoenigma/enigmaz30simulator/blob/master/EnigmaZ30Sim.ino)

Hackaday.IO page:
[https://hackaday.io/project/165684-nano-z30-a-numbers-only-enigma-machine-simulator](https://hackaday.io/project/165684-nano-z30-a-numbers-only-enigma-machine-simulator)

**7/31/2022 Update:**
This simulator has been updated so that the ring setting affects the stepping point. If the ring setting is 1, the wheel steps at the 9 position. Setting it to 2 steps at the 0 position. Setting it to 0 results in the wheel stepping at the 8 position. 

For the default ring setting of 1111, the following is the rotor sequence, which includes a double step of the middle rotors resulting in missing numbers in the sequence. 
0888 -> 0889 -> 0890 -> 0901 -> 1002 -> 1003

For a ring setting of 1112, the rightmost wheel steps the next wheel when 0 is showing. The following is the stepping sequence:
0000 -> 0011 -> 0012 -> 0013 -> 0014 -> 0015 -> 0016 -> 0017 -> 0018 -> 0019 -> 0010 -> 0021 -> 0022 -> 0023

For a ring setting of 1110, the rightmost wheel steps the next wheel when 8 is showing. The following is the stepping sequence:
0000 -> 0001 -> 0002 -> 0003 -> 0004 -> 0005 -> 0006 -> 0007 -> 0008 -> 0019 -> 0010 -> 0011 

The default settings have been changed to:

- Starting wheel position 0000
- Wheel settings: 321
- Ring Settings: 1111


It matches the following online simulator, in which the 0 contacts of the ETW and the rotors are aligned when the rotors are set to 0000

https://people.physik.hu-berlin.de/~palloks/js/enigma/enigma-z_v261_en.html 

// EnigmaZ30Sim.ino - Scroller.ino
// @arduinoenigma 2019


byte scrollup(byte d, byte times)
{
  byte raw = getRaw(d);

  for (byte i = 0; i < times; i++)
  {
    raw = raw << 1;
    raw = raw & B01101010;
  }

  return raw;
}


byte scrolldn(byte d, byte times)
{
  byte raw = getRaw(d);

  for (byte i = 0; i < times; i++)
  {
    raw = raw >> 1;
    raw = raw & B00110101;
  }

  return raw;
}


// scrollstep goes 0..4
byte stepUp(byte src, byte dest, byte scrollstep)
{
  byte raw;

  raw = scrolldn(src, scrollstep) | scrollup (dest, 4 - scrollstep);

  return raw;
}


byte stepDown(byte src, byte dest, byte scrollstep)
{
  byte raw;

  raw = scrolldn(dest, 4 - scrollstep) | scrollup (src, scrollstep);

  return raw;
}

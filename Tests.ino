// EnigmaZ30Sim.ino - Tests.ino
// @arduinoenigma 2019


void testBNumber()
{
  Serial.println(B00000001);
  Serial.println(0b00000001);
  Serial.println(B00000010);
  Serial.println(0b00000010);
  Serial.println(0b10000000000000000);
  Serial.println(0b1000000000000000);
}


//press 2 and 3
void testPressTwoKeys()
{
  KEY1L.input();
  UPDNL.input();
  KEY1L.high();
  UPDNL.high();
  for (int i = 0; i < 9000; i++)
  {
    delayMicroseconds(9000);
    allSegmentHigh();
    SegmentE.low();
    delayMicroseconds(9000);
    Serial.print(KEY1L.read());
    Serial.print(UPDNL.read());
    Serial.println(":2");
    allSegmentOff();
    allSegmentHigh();
    delayMicroseconds(9000);
    SegmentF.low();
    delayMicroseconds(9000);
    Serial.print(KEY1L.read());
    Serial.print(UPDNL.read());
    Serial.println(":3");
    allSegmentOff();
  }
}


void testDisplayKeysPressed()
{
  for (byte i = 0; i < 19; i++)
  {
    Serial.print((char)KeysPressed[i]);
  }
  Serial.print(' ');
  for (byte i = 0; i < 19; i++)
  {
    Serial.print(KeysDebounce[i]);
  }
  Serial.print(' ');
  for (byte i = 0; i < 19; i++)
  {
    Serial.print((char)KeysProcessed[i]);
  }
  Serial.print(' ');
  for (byte i = 0; i < 19; i++)
  {
    Serial.print((char)LightUp[i]);
  }
  Serial.println(' ');
}


void testOneSegment()
{
  Serial.println("hardcoded line test");
  SegmentB.output();
  CCDS1.output();
  for (int i = 0; i < 9000; i++)
  {
    SegmentA.high();
    CCDS1.low();
    delayMicroseconds(300);
    CCDS1.high();
    delayMicroseconds(1800);
  }
  Serial.println("done");
}


// scrollstep goes 0..4
byte testStepUp(byte src, byte dest, byte scrollstep)
{
  byte raw;

  raw = scrolldn(src, scrollstep) | scrollup (dest, 4 - scrollstep);

  return raw;
}


byte testStepDown(byte src, byte dest, byte scrollstep)
{
  byte raw;

  raw = scrolldn(dest, 4 - scrollstep) | scrollup (src, scrollstep);

  return raw;
}


void testScroller()
{
  for (byte i = 0; i < 5; i++)
  {
    displayRaw(testStepUp(9, 0, i));

    for (int i = 0; i < 100; i++)
    {
      selectDigit(4);
      delayMicroseconds(300);
      selectDigit(99);
      delayMicroseconds(1800);
    }
  }

  for (byte i = 0; i < 5; i++)
  {
    displayRaw(testStepDown(0, 9, i));

    for (int i = 0; i < 100; i++)
    {
      selectDigit(4);
      delayMicroseconds(300);
      selectDigit(99);
      delayMicroseconds(1800);
    }
  }
}


//this function scrolls the next higher number from underneath the current one
void testDisplay0to9()
{
  for (byte j = 0; j < 10; j++) // j<10 to generate complete table, j<9 for normal display
  {
    for (byte i = 0; i < 5; i++)
    {
      displayRaw(scrolldn(j, i) | scrollup((j == 9) ? 0 : j + 1, 4 - i)); // to generate complete table

      //displayRaw(scrollup(j, i) | scrolldn(j + 1, 4 - i));
      //display(scrollup(j, i) | scrolldn((j == 9) ? 0 : j + 1, 4 - i)); // to generate complete table

      for (int i = 0; i < 100; i++)
      {
        selectDigit(4);
        delayMicroseconds(300);
        selectDigit(99);
        delayMicroseconds(1800);
      }
    }
  }

  for (byte j = 9; j > 0; j--)
  {
    for (byte i = 0; i < 5; i++)
    {
      displayRaw(scrolldn(j - 1, 4 - i) | scrollup ((j == 9) ? 0 : j, i)); // to generate complete table
      //displayRaw(scrolldn(j, i) | scrollup((j == 9) ? 0 : j + 1, 4 - i)); // to generate complete table
      //displayRaw(scrollup(j, i) | scrolldn(j + 1, 4 - i));
      //display(scrollup(j, i) | scrolldn((j == 9) ? 0 : j + 1, 4 - i)); // to generate complete table

      for (int i = 0; i < 100; i++)
      {
        selectDigit(4);
        delayMicroseconds(300);
        selectDigit(99);
        delayMicroseconds(1800);
      }
    }
  }
}

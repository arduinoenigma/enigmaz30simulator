// EnigmaZ30Sim.ino - Timer.ino
// @arduinoenigma 2019

unsigned long exectimer[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned long maxtimes[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


// the timing functions add 10.6uS of overhead to caller
void entrytimer(byte task)
{
  exectimer[task] = micros();
}


void exittimer(byte task)
{
  unsigned long now = micros();
  unsigned long entry = exectimer[task];

  unsigned long t = now - entry;

  if (t > maxtimes[task])
  {
    maxtimes[task] = t;
  }
}


void printtimer()
{
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(i);
    Serial.print(F(":"));
    Serial.println(maxtimes[i]);
  }
}
